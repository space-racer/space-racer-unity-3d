﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    private CarControl carControl;
    private float rearFactor = 0.5f;

	void Start () {
        carControl = GetComponent<CarControl>();
	}
	
	void Update () {

        //Motor (priority on front)
        if (Input.GetAxis(InputsMapping.Axis(XboxInputAxis.RightTrigger)) > 0)
        {
            //Front Motor (priority on front)
            carControl.SetMotor(Input.GetAxis(InputsMapping.Axis(XboxInputAxis.RightTrigger)));
        }
        else
        {
            //Rear Motor
            carControl.SetMotor(-rearFactor * Input.GetAxis(InputsMapping.Axis(XboxInputAxis.LeftTrigger)));
        }
        
        //Sterring
        carControl.SetSteering(Input.GetAxis(InputsMapping.Axis(XboxInputAxis.LeftStickX)));
    }

    private void FixedUpdate()
    {
        if(Input.GetKeyDown(InputsMapping.Button(XboxInputButton.ButtonY)))
        {
            carControl.RotateToRoadForward();
        }
    }

}
