﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundariesDestroy : MonoBehaviour {

    private GameObject player;
    private GameManager sc_gameManager;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        GameObject gameManager = GameObject.FindGameObjectWithTag("GameController");
        if(gameManager != null)
            sc_gameManager = gameManager.GetComponent<GameManager>();
    }


	void FixedUpdate () {
		if(Input.GetKeyDown(InputsMapping.Button(XboxInputButton.ButtonX)))
        {
            CarControl cc = player.GetComponent<CarControl>();
            cc.RespawnAt(sc_gameManager.playerRespawn.position);

            sc_gameManager.DeathLosePoint();
        }
	}


    private void OnTriggerExit(Collider other)
    {
        if(other.transform.root.CompareTag("Player"))
        {
            CarControl cc = other.GetComponent<CarControl>();
            cc.RespawnAt(sc_gameManager.playerRespawn.position);

            sc_gameManager.DeathLosePoint();
        }
        else if (other.transform.root.CompareTag("Enemy"))
        {
            CarControl cc = other.GetComponent<CarControl>();
            cc.RespawnAt(sc_gameManager.GetRandomEnemySpawnPoint());

            sc_gameManager.KillWinPoint();
        }
        else
        {
            Destroy(other.gameObject);
        }
    }
}
