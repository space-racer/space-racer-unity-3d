﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : MonoBehaviour {

    public static float steeringFactor = 0.9f;
    public static float scopeDistance = 100;
    public static float forseenPositionFactor = 10;

    public static float forwardFactor = 20;

    public static float edgeRepulsion = 30;
    public static float maximalDistanceFromCenter = 65;

    public static float minimalCrewDistance = 15;
    public static float crewRepulsion = 15;
    public static float crewAttraction = 10;

    public static float playerAttraction = 10;

    private CarControl carControl;
    private Rigidbody rb;

    private GameObject road;
    private GameObject player;
    private Rigidbody rbPlayer;

    private bool AutoRespawn = false;
    private int AutoRespawnDelaySeconds = 5;
    private float AutoRespawnVelocityTrigger = 3;

    void Start () {
        carControl = GetComponent<CarControl>();
        rb = GetComponent<Rigidbody>();

        road = GameObject.FindGameObjectWithTag("Road");
        player = GameObject.FindGameObjectWithTag("Player");
        rbPlayer = player.GetComponent<Rigidbody>();
    }
	

	void FixedUpdate () {
        if(!AutoRespawn && rb.velocity.magnitude < AutoRespawnVelocityTrigger)
        {
            StartCoroutine(AutoRespawnWithDelay());
        }

        Vector3 targetMovement = Vector3.zero;

        targetMovement += MoveForeward();
        targetMovement += EdgeRepulse();
        targetMovement += PushPlayer();

        ApplyMovement(targetMovement);
    }


    //APPLY VECTOR TO CAR CONTROL
    void ApplyMovement(Vector3 targetMovement)
    {
        //Projection of the target movement on the plane of the car, this is necessary to calculate steering and motor
        Vector3 projectedVector = Vector3.ProjectOnPlane(targetMovement, transform.up);


        //Motor and steering between -1 et 1 (proportion of maxSpeed / maxAngle)

        //Steering takes into account the difference between the desired direction and the current one
        float steering = steeringFactor * Vector3.SignedAngle(transform.forward, projectedVector.normalized, transform.up) / carControl.maxSteeringAngle;

        //Motor takes steering into account
        float motor = Vector3.Dot(projectedVector.normalized, transform.forward);

        carControl.SetMotor(motor);
        carControl.SetSteering(steering);
    }

    //CALCULATING MOVEMENT
    Vector3 MoveForeward()
    {
        return forwardFactor * RoadForward();
    }

    Vector3 EdgeRepulse()
    {
        Vector3 roadCenter = road.transform.position;
        Vector3 forseenPosition = transform.position + forseenPositionFactor * rb.velocity * Time.fixedDeltaTime;

        float distanceFromCenter = roadCenter.x - forseenPosition.x;
        if (Mathf.Abs(distanceFromCenter) > maximalDistanceFromCenter)
            return edgeRepulsion * (distanceFromCenter - maximalDistanceFromCenter) * Vector3.right;
        else
            return Vector3.zero;
    }

    Vector3 PushPlayer()
    {
        
        if (Distance(player) < scopeDistance)
        {
            Vector3 roadCenter = road.transform.position;

            Vector3 playerNextPosition = player.transform.position + forseenPositionFactor * rbPlayer.velocity * Time.fixedDeltaTime;

            if (Mathf.Abs(roadCenter.x - playerNextPosition.x) < maximalDistanceFromCenter)
                return playerAttraction * (playerNextPosition - transform.position);

        }   
        
        return Vector3.zero;
    }

    Vector3 FormCrew()
    {
        Vector3 formCrew = Vector3.zero;

        List<GameObject> crewInScope = NearCrew();

        if(crewInScope.Count > 0)
        {
            foreach (GameObject car in crewInScope)
            {
                formCrew += car.transform.position - transform.position;
            }
            formCrew /= crewInScope.Count;
        }
        
        return crewAttraction * formCrew;
    }

    Vector3 AvoidCrewMembers()
    {
        Vector3 avoidCrew = Vector3.zero;

        List<GameObject> tooCloseCrew = TooCloseCrew();

        if (tooCloseCrew.Count > 0)
        {
            foreach (GameObject car in tooCloseCrew)
            {
                avoidCrew += transform.position - car.transform.position;
            }
            avoidCrew /= tooCloseCrew.Count;
        }

        return crewRepulsion * avoidCrew;
    }



    //USEFULL FUNCTIONS
    List<GameObject> NearCrew()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        List<GameObject> nearEnemies = new List<GameObject>();

        foreach (GameObject other in enemies)
        {
            if (other != gameObject && IsInScope(other))
            {
                nearEnemies.Add(other);
            }
        }

        return nearEnemies;
    }

    List<GameObject> TooCloseCrew()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        List<GameObject> tooCloseEnemies = new List<GameObject>();

        foreach (GameObject other in enemies)
        {
            if (other != gameObject && Distance(other) < minimalCrewDistance)
            {
                tooCloseEnemies.Add(other);
            }
        }

        return tooCloseEnemies;
    }


    float Distance(GameObject other)
    {
        return Vector3.Distance(transform.position, other.transform.position);
    }

    bool IsInScope(GameObject other)
    {
        return Distance(other) < scopeDistance;
    }

    Vector3 RoadForward()
    {
        //Forward direction is calculated with the vector up of the car and 
        return Vector3.Cross(road.transform.right, transform.up);
    }

    IEnumerator AutoRespawnWithDelay()
    {
        AutoRespawn = true;

        int i;
        for(i = 1; i <= AutoRespawnDelaySeconds; ++i)
        {
            if (rb.velocity.magnitude < AutoRespawnVelocityTrigger)
            {
                    yield return new WaitForSeconds(1);
            }
            else
                break;
        }

        if (i >= AutoRespawnDelaySeconds)
        {
            Debug.Log("Enemy auto repawn");
            carControl.RespawnAt(transform.position - transform.position.x * road.transform.right);
        }

        AutoRespawn = false;
    }
}
