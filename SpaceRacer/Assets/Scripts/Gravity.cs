﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour {

    private GameObject road;
    [Range(1f, 20f)]
    public float g = 9.8f;
    private Vector3 center;

    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        road = GameObject.FindGameObjectWithTag("Road");
        center = road.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 gravityDirection = Vector3.Normalize(transform.position - center);
        rb.AddForce(rb.mass * g * gravityDirection);
	}
}
