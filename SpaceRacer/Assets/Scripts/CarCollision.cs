﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCollision : MonoBehaviour {

    //private Rigidbody rbCar;

    public AudioSource impactSound;

    void Awake ()
    {
        //rbCar = GetComponent<Rigidbody>();
    }

	// Use this for initialization
    void OnCollisionEnter(Collision collision)
    {
        Rigidbody rbOther = collision.gameObject.GetComponent<Rigidbody>();
        if (rbOther != null)
        {
            Debug.Log("impact!");
            //rbOther.AddForce(- collision.impulse / Time.fixedDeltaTime, ForceMode.Impulse);
            //rbOther.AddForce((rbCar.velocity - rbOther.velocity) * 1000, ForceMode.Impulse);
            //collision.rigidbody.velocity += (collision.impulse / 10 + rbCar.velocity) * -1;
            if(!impactSound.isPlaying)
            {
                impactSound.Play();
            }
        }
    }
}
