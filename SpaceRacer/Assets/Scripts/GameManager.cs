﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    private GameObject player;
    private Rigidbody rbPlayer;

    private GameObject road;

    //SPAWN POINTS
    public float spawnSideMax;
    public int n_enemyRespawns;
    [HideInInspector]
    public Transform playerRespawn;
    private Transform enemyRespawn;

    //GAME DATA
    private volatile float score;
    
    //UI
    public Text T_scoreDisplayer;
    public Text T_speed;
    private Color speedColor;

    

	// Use this for initialization
	void Start () {
        score = 0;
        player = GameObject.FindGameObjectWithTag("Player");
        rbPlayer = player.GetComponent<Rigidbody>();
        road = GameObject.FindGameObjectWithTag("Road");
        playerRespawn = transform.GetChild(0).transform;
        enemyRespawn = transform.GetChild(1).transform;
    }
	
	// Update is called once per frame
	void Update () {
        ScoreManager();
        SpeedDisplayer();
    }


    void ScoreManager()
    {
        T_scoreDisplayer.text ="score : " + Mathf.Round(score * 100).ToString();
        score += Time.deltaTime * rbPlayer.velocity.magnitude / 100;
    }

    void SpeedDisplayer()
    {
        T_speed.text = Mathf.Round(rbPlayer.velocity.magnitude) + " mph";
        speedColor = new Color(255f/255f,(255-rbPlayer.velocity.magnitude)/255f, 40/255f);
        T_speed.color = speedColor;
    }

    public void DeathLosePoint()
    {
        score -= 10;
    }

    public void KillWinPoint()
    {
        score += 10;
    }

    public Vector3 GetRandomEnemySpawnPoint()
    {
        enemyRespawn.position = playerRespawn.position;

        //Enemy can spawn at different parts of the road (sideway)
        enemyRespawn.Translate(road.transform.right * Random.Range(-spawnSideMax, spawnSideMax));

        //Enemy can spawn at different parts of the road (section) (except the spawn position of the player)
        enemyRespawn.RotateAround(road.transform.position, road.transform.right, Random.Range(1, n_enemyRespawns) * 360f / n_enemyRespawns);
        
        //If player car is too close - go to the opposite part of the road
        if((player.transform.position - enemyRespawn.position).magnitude < 300)
            enemyRespawn.RotateAround(road.transform.position, road.transform.right, 180);

        return enemyRespawn.position;
    }
}
