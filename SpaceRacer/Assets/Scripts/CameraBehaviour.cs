﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour {

    private Transform TR_Camera;
    private Camera mainCamera;

    private GameObject car;
    private Rigidbody RB_rdb;

    private float rot = 0;
    private float dist = 0;
    public float maxrot;
    public float maxdist;
    private float speedmove;
    public float multiplyspeed;
    public float multiplyreturn;

    // Use this for initialization
    void Start () {
        mainCamera = GetComponent<Camera>();
        //mainCamera = Camera.main;
        TR_Camera = GetComponent<Transform>();
        car = GameObject.FindGameObjectWithTag("Player");
        RB_rdb = car.GetComponent<Rigidbody>();

    }
	
	// Update is called once per frame
	void Update ()
    {
        speedmove = RB_rdb.velocity.magnitude;
        
        
        CameraAngle();
        FovChanger();
    }

    private void FovChanger()
    {
        mainCamera.fieldOfView = Mathf.Clamp(50 + speedmove / 3, 50f, 90f);
    }

    void CameraAngle()
    {

        //Debug.Log(rot);

        if (Input.GetAxis(InputsMapping.Axis(XboxInputAxis.LeftStickX)) < 0)
        {
            if (rot < 0)
                rot -= Mathf.Lerp(0, Mathf.Sqrt(speedmove) * multiplyspeed * multiplyreturn, 0.5f);
            else
                rot -= Mathf.Lerp(0, Mathf.Sqrt(speedmove) * multiplyspeed, 0.5f);
            //if (rot < -maxrot)
            //    rot = -maxrot;


            if (dist > 0)
                dist -= Mathf.Lerp(0, Mathf.Sqrt(speedmove) * multiplyspeed * multiplyreturn, 0.5f);
            else
                dist -= Mathf.Lerp(0, Mathf.Sqrt(speedmove) * multiplyspeed, 0.5f);

            //if (dist < -maxdist)
            //    dist = -maxdist;

            TR_Camera.localRotation = (Quaternion.Euler(11, rot, 0));
            TR_Camera.localPosition = new Vector3(dist, 4.5f,-11);
        }
        else if (Input.GetAxis(InputsMapping.Axis(XboxInputAxis.LeftStickX)) > 0)
        {

            if (rot > 0)
                rot += Mathf.Lerp(0, Mathf.Sqrt(speedmove) * multiplyspeed * multiplyreturn, 0.5f);
            else
                rot += Mathf.Lerp(0, Mathf.Sqrt(speedmove) * multiplyspeed, 0.5f);
            //if (rot > maxrot) 
            //    rot = maxrot;


            if (dist < 0)
                dist += Mathf.Lerp(0, Mathf.Sqrt(speedmove) * multiplyspeed * multiplyreturn, 0.5f);
            else
                dist += Mathf.Lerp(0, Mathf.Sqrt(speedmove) * multiplyspeed, 0.5f);
             //if (dist > maxdist)
            //    dist = maxdist;

            TR_Camera.localRotation = Quaternion.Euler(11, rot, 0);
            TR_Camera.localPosition = new Vector3(dist, 4.5f, -11);
        }

        else
        {
            if (dist < 0 && dist != 0)
                dist += Mathf.Lerp(0, Mathf.Sqrt(speedmove) * multiplyspeed * multiplyreturn, 0.5f);
            if (dist > 0 && dist != 0)
                dist -= Mathf.Lerp(0, Mathf.Sqrt(speedmove) * multiplyspeed * multiplyreturn, 0.5f);

            if (rot < 0 && rot != 0)
                rot += Mathf.Lerp(0, Mathf.Sqrt(speedmove) * multiplyspeed * multiplyreturn, 0.5f);
            if (rot > 0 && rot != 0)
                rot -= Mathf.Lerp(0, Mathf.Sqrt(speedmove) * multiplyspeed * multiplyreturn, 0.5f);
            TR_Camera.localRotation = Quaternion.Euler(11, rot, 0);
            TR_Camera.localPosition = new Vector3(dist, 4.5f, -11);
        }
        rot = Mathf.Clamp(rot, -maxrot, maxrot);
        dist = Mathf.Clamp(dist, -maxdist, maxdist);
    }
}
