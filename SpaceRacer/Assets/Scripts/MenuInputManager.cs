﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuInputManager : MonoBehaviour {

    // Menu state
    bool inMainMenu = true;
    bool inGameMenu = false;
    public Canvas InGameMenu;

    //MUSIC
    private MusicManager musicManager;
    

    void Start()
    {
        if (InGameMenu != null)
            inMainMenu = false;

        musicManager = GameObject.FindGameObjectWithTag("Audio").GetComponent<MusicManager>();
    }

    void Update()
    {
        if (inMainMenu)
        {
            if (Input.GetKeyDown(InputsMapping.Button(XboxInputButton.StartButton)))
            {
                inMainMenu = false;
                SceneManager.LoadScene(1);
                musicManager.playGameLoop = true;
            }
            else if (Input.GetKeyDown(InputsMapping.Button(XboxInputButton.BackButton)))
            {
                Application.Quit();
            }
        }
        else if (inGameMenu)
        {
            if (Input.GetKeyDown(InputsMapping.Button(XboxInputButton.StartButton)))
            {
                Time.timeScale = 1;
                inGameMenu = false;
            }
            else if (Input.GetKeyDown(InputsMapping.Button(XboxInputButton.BackButton)))
            {
                Time.timeScale = 1;
                SceneManager.LoadScene(0);
                inMainMenu = true;
                inGameMenu = false;
                musicManager.playGameLoop = false;
            }
        }
        else
        {
            if (Input.GetKeyDown(InputsMapping.Button(XboxInputButton.StartButton)))
            {
                Time.timeScale = 0;
                inGameMenu = true;
            }
        }

        ShowMenuIngame();
    }

    void ShowMenuIngame()
    {
        if (InGameMenu != null)
        {
            if (Time.timeScale == 0)
                InGameMenu.enabled = true;
            else
                InGameMenu.enabled = false;
        }
    }
}
