﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour {


    public AudioSource klaxon;
    public AudioSource bark;
    public List<AudioClip> barkList;

    private int barkIndex;


    void Start () {
        barkIndex = 0;
	}
	

	void FixedUpdate () {
        if (Input.GetKeyDown(InputsMapping.Button(XboxInputButton.ButtonB)))
        {
            if (!klaxon.isPlaying)
                klaxon.Play();

            if(!bark.isPlaying)
            {
                bark.clip = GetRandomBark();
                bark.Play();
            }
        }
    }

    AudioClip GetRandomBark()
    {
        if(barkIndex >= barkList.Count)
        {
            ShuffleBark();
            barkIndex = 0;
        }

        AudioClip barkClip = barkList[barkIndex];
        ++barkIndex;
        return barkClip;
    }

    void ShuffleBark()
    {
        AudioClip lastPlayedAudio = barkList[barkList.Count - 1];
        //Shuffle
        for(int n = barkList.Count - 1; n > 0; --n)
        {
            int k = Random.Range(0, n + 1);
            AudioClip tempClip = barkList[k];
            barkList[k] = barkList[n];
            barkList[n] = tempClip;
        }

        //If last played audio is the same as the first of the list, switch it with another
        if(lastPlayedAudio == barkList[0])
        {
            int k = Random.Range(1, barkList.Count + 1);
            AudioClip tempClip = barkList[k];
            barkList[k] = barkList[0];
            barkList[0] = tempClip;
        }
    }
}
