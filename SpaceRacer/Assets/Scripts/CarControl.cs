﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CarControl : MonoBehaviour
{
    public List<AxleInfo> axleInfos; // the information about each individual axle
    public float maxMotorTorque; // maximum torque the motor can apply to wheel
    public float maxSteeringAngle; // maximum steer angle the wheel can have

    private float motor = 0;
    private float steering = 0;

    private GameObject road;
    private Vector3 roadCenter;

    private Rigidbody rb;

    public AudioSource respawn;

    private void Start()
    {
        road = GameObject.FindGameObjectWithTag("Road");
        rb = GetComponent<Rigidbody>();
        roadCenter = road.transform.position;
    }

    public void FixedUpdate()
    {
        foreach (AxleInfo axleInfo in axleInfos)
        {
            if (axleInfo.steering)
            {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor)
            {
                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;
            }

            ApplyLocalPositionToVisuals(axleInfo.leftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel);
        }

    }

    //Proportion (from -1 to 1)
    public void SetMotor(float proportion)
    {
        motor = Mathf.Clamp(proportion, -1, 1) * maxMotorTorque;
    }

    //Proportion (from -1 to 1)
    public void SetSteering(float proportion)
    {
        steering = Mathf.Clamp(proportion, -1, 1) * maxSteeringAngle;
    }

    // finds the corresponding visual wheel
    // correctly applies the transform
    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {
        if (collider.transform.childCount == 0)
        {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

    //Useful, to get the right direction
    public Vector3 RoadForward()
    {
        //Forward direction is calculated with the vector up of the car and 
        return Vector3.Cross(road.transform.right, transform.up);
    }

    //For spawn, to be sure the car is in the right direction
    public void RotateToRoadForward()
    {
        transform.rotation = Quaternion.identity;
        transform.rotation *= Quaternion.FromToRotation(transform.up, Vector3.ProjectOnPlane(roadCenter - transform.position, road.transform.right));
        transform.rotation *= Quaternion.FromToRotation(transform.forward, RoadForward());
    }

    public void RespawnAt(Vector3 position)
    {
        //Reset velocity
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        //True position and true rotation
        transform.position = position;
        RotateToRoadForward();

        respawn.Play();
    }
}

[System.Serializable]
public class AxleInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor; // is this wheel attached to motor?
    public bool steering; // does this wheel apply steer angle?
}