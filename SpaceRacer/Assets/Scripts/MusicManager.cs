﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MusicManager : MonoBehaviour {

    private static MusicManager instance = null;

    public AudioSource mainLoop;
    public AudioSource gameLoop;

    private double nextEventTime;

    public bool playGameLoop = false;

    void Awake () {
        if (instance != null && instance != this)
            Destroy(gameObject);
        else
            instance = this;

        DontDestroyOnLoad(gameObject);

        nextEventTime = AudioSettings.dspTime + 0.2f;
	}

    private void Update()
    {
        double time = AudioSettings.dspTime;

        if (!playGameLoop)
        {
            gameLoop.Stop();
        }

        if (time + 1.0f > nextEventTime)
        {
            if (!mainLoop.isPlaying)
            {
                mainLoop.PlayScheduled(nextEventTime);
            }

            if (playGameLoop && !gameLoop.isPlaying)
            {
                gameLoop.PlayScheduled(nextEventTime);
            }

            

            nextEventTime += mainLoop.clip.length;
        }
        
    }

}
